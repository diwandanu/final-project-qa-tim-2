<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Beli                                     _1a5f60 (1)</name>
   <tag></tag>
   <elementGuidId>8a00f69d-686a-4465-ae13-79e64d6ac9b1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#addCart</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='addCart']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>46b04f0d-50fb-411e-b3ca-054c7948043b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>addCart</value>
      <webElementGuid>bf9106a1-4a10-464f-8cbb-7e79cf99492b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary btn-lg</value>
      <webElementGuid>70692c29-8892-419d-a1cb-cd19c4874aa7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Beli
                                                    Tiket</value>
      <webElementGuid>1d423e97-a3ba-4eff-845d-d4fff1525906</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;addCart&quot;)</value>
      <webElementGuid>43c607c2-7dd7-4f47-a771-3d750fe19ffc</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@id='addCart']</value>
      <webElementGuid>d2eb76d9-408f-40f3-8ef8-c70a6201cac7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='list-button']/a</value>
      <webElementGuid>7316a897-125c-4e79-9f2e-a32dec7869f2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Beli
                                                    Tiket')]</value>
      <webElementGuid>61b3bcba-ebf8-484d-933d-264f1817af55</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Detik'])[1]/following::a[1]</value>
      <webElementGuid>5f4cd635-6195-4c88-be8c-8d9d26ae19a1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Menit'])[1]/following::a[1]</value>
      <webElementGuid>2a9d2c84-5c93-4576-a1c7-4c7e2b85631d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Event lain yang banyak diminati'])[1]/preceding::a[1]</value>
      <webElementGuid>58462455-9559-4043-b612-535ecc8ec26a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Day 3: Predict using Machine Learning'])[1]/preceding::a[1]</value>
      <webElementGuid>b2757c2e-4724-408d-8aa6-2f550533b799</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//aside/div[2]/ul/li[6]/a</value>
      <webElementGuid>bdcee2e1-b601-4422-8024-01c3edf83405</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@id = 'addCart' and (text() = 'Beli
                                                    Tiket' or . = 'Beli
                                                    Tiket')]</value>
      <webElementGuid>8a1b585a-324c-4088-a8f4-f21b68060637</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
