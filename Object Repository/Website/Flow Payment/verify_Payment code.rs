<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>verify_Payment code</name>
   <tag></tag>
   <elementGuidId>4a90a6a6-729b-45a6-aa27-45edcd879403</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='application']/div/div[3]/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.payment-page-layout.payment-page-text > div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>ef4be21b-3493-4e4a-9cac-386f06ff0c94</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Payment code</value>
      <webElementGuid>2c7436cc-52b6-4511-a176-c538287cd825</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;application&quot;)/div[@class=&quot;page-container scroll&quot;]/div[@class=&quot;payment-page-layout payment-page-text&quot;]/div[1]</value>
      <webElementGuid>526f95f4-1009-4a33-b706-9ef50438a59e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Website/Flow Payment/iframe_concat(id(, , snap-midtrans, , ))_popup_1675685834963</value>
      <webElementGuid>75b4f5c9-b56b-4cf3-99b0-8ed106a6eb9d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='application']/div/div[3]/div</value>
      <webElementGuid>1a8cd7c8-0960-4352-897b-b17b233d692e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Show barcode to the cashier or enter payment code on i.saku app.'])[1]/following::div[2]</value>
      <webElementGuid>bf6e06bf-cde2-4247-adee-d2b1fa05ab23</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Indomaret'])[1]/following::div[3]</value>
      <webElementGuid>c110a59d-6a83-4dd4-b0a5-f250c4c788cc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Copy'])[1]/preceding::div[2]</value>
      <webElementGuid>35dc7ca4-a219-409e-9128-84904ed255c7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='How to pay'])[1]/preceding::div[4]</value>
      <webElementGuid>69f0689f-f58a-4f3b-b6f8-e595f27af8d6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Payment code']/parent::*</value>
      <webElementGuid>e0145086-d190-4bf7-b0c4-02218cfbcddf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[3]/div</value>
      <webElementGuid>c37c439a-97f4-45b1-afe2-ad2d4496b4aa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Payment code' or . = 'Payment code')]</value>
      <webElementGuid>2a35a6bb-9476-4178-8d99-2bebedb98031</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
