<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>verify_Pilih minimal 1 event sebelum checkout</name>
   <tag></tag>
   <elementGuidId>f0037fa5-b55b-4a84-8f2c-45f3a0b30d54</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='modalEvent']/div/div/div[2]/p</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.modal-body > p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>43fc5393-5cb8-4c62-8c37-256b970c623e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Pilih minimal 1 event sebelum checkout</value>
      <webElementGuid>1eb19469-8366-41d0-aab9-e5994dd3fa6c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;modalEvent&quot;)/div[@class=&quot;modal-dialog&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body&quot;]/p[1]</value>
      <webElementGuid>f270247d-cfad-4ad3-929f-6b4b3992e9cf</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='modalEvent']/div/div/div[2]/p</value>
      <webElementGuid>f4f2ce79-e7a5-4e9d-8aec-c11854f88407</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Information'])[1]/following::p[1]</value>
      <webElementGuid>8baca072-1e7e-4342-a930-48f510b235b9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Confirm'])[1]/following::p[1]</value>
      <webElementGuid>e505ae02-b8c1-4027-ac81-0007e0f06ef3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ok'])[1]/preceding::p[1]</value>
      <webElementGuid>fbc0e25e-7505-46cd-ba7d-332898cdb838</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Copyright © 2010-2021 Coding.ID All rights reserved.'])[1]/preceding::p[1]</value>
      <webElementGuid>842007e1-c0c5-40cd-965f-4287bf78830a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Pilih minimal 1 event sebelum checkout']/parent::*</value>
      <webElementGuid>d306df60-8902-40d6-970b-c35dd6bafe89</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div[2]/p</value>
      <webElementGuid>1e6c2609-8407-4dca-98f1-8f80f2f38166</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'Pilih minimal 1 event sebelum checkout' or . = 'Pilih minimal 1 event sebelum checkout')]</value>
      <webElementGuid>ddf9c38b-8af5-4db6-8587-564d760156e2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
