<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Lihat                Pembelian Saya</name>
   <tag></tag>
   <elementGuidId>9308d48a-9954-4325-8f9e-dbbfb4743f3f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#checkoutButton</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='checkoutButton']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>04a3c603-42f1-4505-83ee-e1e65e12ca76</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/view_cart/130</value>
      <webElementGuid>473d5090-c787-4328-aeaf-c8e9b0009c93</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>checkoutButton</value>
      <webElementGuid>ede4bf05-d948-47a4-bbed-23bc2ccd0d9e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Lihat
                Pembelian Saya</value>
      <webElementGuid>137991e9-8e0e-47cc-88df-5df523c91ae4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;checkoutButton&quot;)</value>
      <webElementGuid>c55845d6-1e91-45e5-b927-2a313d2d7b7d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@id='checkoutButton']</value>
      <webElementGuid>f1bd9889-f078-4b8c-a957-a5d9c92f2e45</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='Modal_Success']/div/div[2]/a</value>
      <webElementGuid>9fc34489-0fab-4ac6-b9c7-8ebb72c0e632</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Lihat
                Pembelian Saya')]</value>
      <webElementGuid>5893c50d-4c35-4d2c-a14a-ad80a33b37a8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Day 4: Workshop'])[4]/following::a[1]</value>
      <webElementGuid>40cce1a6-8354-423f-aff0-b7bcb5dd6ab2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='add to cart success'])[1]/following::a[1]</value>
      <webElementGuid>a8c7892e-d649-4a4b-a8a1-5af43b7543d7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lihat Event Lainnya'])[1]/preceding::a[1]</value>
      <webElementGuid>9343fbd6-f03e-4a12-a81e-94e2cb23f5eb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Copyright © 2010-2021 Coding.ID All rights reserved.'])[1]/preceding::a[2]</value>
      <webElementGuid>8472e45f-00c9-4118-ab12-6dec1d6b2360</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/view_cart/130')]</value>
      <webElementGuid>cfdd528f-a61c-4f38-befa-d63c703e40df</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]/div/div[2]/a</value>
      <webElementGuid>dc6eff6d-2384-4d0e-9733-a1911040f06a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/view_cart/130' and @id = 'checkoutButton' and (text() = 'Lihat
                Pembelian Saya' or . = 'Lihat
                Pembelian Saya')]</value>
      <webElementGuid>57446cbe-15c9-43ea-9cde-947e1f8562ee</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Day 3: Predict using Machine Learning'])[3]/following::a[1]</value>
      <webElementGuid>5cf37932-cecd-4d6b-b3dd-fd2baf455a3b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/view_cart/129')]</value>
      <webElementGuid>9598344f-7569-4ade-b322-44a559544458</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/view_cart/129' and @id = 'checkoutButton' and (text() = 'Lihat
                Pembelian Saya' or . = 'Lihat
                Pembelian Saya')]</value>
      <webElementGuid>ae9af322-0e7c-4d1a-986e-3349d2470525</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
