<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_TEST_modal-iframe (1)</name>
   <tag></tag>
   <elementGuidId>8599438f-1a85-4778-b6c9-ad822b0a5859</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.modal-iframe</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//body[@id='snap-body']/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>48b66b9d-34ba-44f9-99f9-511f3c1bd3a9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>modal-iframe</value>
      <webElementGuid>764de364-54e0-4c4f-8035-0d40c6d4bb3f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;snap-body&quot;)/div[@class=&quot;modal-iframe&quot;]</value>
      <webElementGuid>2140e88b-ba43-430d-adc2-da0310f3968c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Website/Flow Payment/iframe_concat(id(, , snap-midtrans, , ))_po_af3316</value>
      <webElementGuid>71695327-4e7e-4e90-8767-24059191d7b4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//body[@id='snap-body']/div</value>
      <webElementGuid>3e270d2e-27ee-4710-abb4-ec7c8f66367a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div</value>
      <webElementGuid>52a028bf-01e2-434e-a7e8-1af7ca6e34be</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
