<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>iframe_concat(id(, , snap-midtrans, , ))_po_aadc16</name>
   <tag></tag>
   <elementGuidId>dcb639dd-67e7-4260-8773-fdf5207e094c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#snap-midtrans</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//iframe[@id='snap-midtrans']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>iframe</value>
      <webElementGuid>6e118d80-0326-428b-a1b2-ba2764b404b5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>https://app.sandbox.midtrans.com/snap/v1/pay?origin_host=https://demo-app.online&amp;digest=4a769263444bfda2746d29828f5ebbf866705895675be67b1319032ea2fe6ff2&amp;client_key=SB-Mid-client-aQuCzf_xly4hqcSY#/</value>
      <webElementGuid>d5c96c9a-4f3b-4895-850b-57b31d07d77e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>snap-midtrans</value>
      <webElementGuid>6473c139-0f9f-4374-8ee9-5aaf0d0b7d8e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>popup_1675789417577</value>
      <webElementGuid>21e3093a-c2de-460e-a66f-482b33200eb1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;snap-midtrans&quot;)</value>
      <webElementGuid>2bde49d6-9534-4005-8993-bd4a7f30684b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//iframe[@id='snap-midtrans']</value>
      <webElementGuid>9048606b-4682-4f08-a062-339bcd862a2e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//iframe</value>
      <webElementGuid>1534588c-5470-47d3-bbac-10d11df3742a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//iframe[@src = 'https://app.sandbox.midtrans.com/snap/v1/pay?origin_host=https://demo-app.online&amp;digest=4a769263444bfda2746d29828f5ebbf866705895675be67b1319032ea2fe6ff2&amp;client_key=SB-Mid-client-aQuCzf_xly4hqcSY#/' and @id = 'snap-midtrans' and @name = 'popup_1675789417577']</value>
      <webElementGuid>c21bc082-7d0c-4529-9639-f7dfa68acb7c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
