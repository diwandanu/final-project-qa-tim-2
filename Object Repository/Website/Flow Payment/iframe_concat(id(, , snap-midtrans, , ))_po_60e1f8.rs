<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>iframe_concat(id(, , snap-midtrans, , ))_po_60e1f8</name>
   <tag></tag>
   <elementGuidId>72eb06e4-6207-4e97-ab25-1eff9d705e89</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//iframe[@id='snap-midtrans']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#snap-midtrans</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>iframe</value>
      <webElementGuid>2d5e7311-b9ba-4ea8-9e8e-0b2540726c6e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>https://app.sandbox.midtrans.com/snap/v1/pay?origin_host=https://demo-app.online&amp;digest=4a769263444bfda2746d29828f5ebbf866705895675be67b1319032ea2fe6ff2&amp;client_key=SB-Mid-client-aQuCzf_xly4hqcSY#/</value>
      <webElementGuid>81d56367-3ff7-4e36-9e53-3b5d9c5367d5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>snap-midtrans</value>
      <webElementGuid>751d0d04-d141-443f-9a1d-b89132e1e805</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>popup_1675684101759</value>
      <webElementGuid>94e4746e-dec3-4e0d-9f7b-42310f388e36</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;snap-midtrans&quot;)</value>
      <webElementGuid>f39ff08f-627d-4df4-995e-74496e8388e2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//iframe[@id='snap-midtrans']</value>
      <webElementGuid>a533d0c7-537a-4abf-b82b-81a1917a03b6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//iframe</value>
      <webElementGuid>64d139bb-2504-4072-8baf-db5c784d64c3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//iframe[@src = 'https://app.sandbox.midtrans.com/snap/v1/pay?origin_host=https://demo-app.online&amp;digest=4a769263444bfda2746d29828f5ebbf866705895675be67b1319032ea2fe6ff2&amp;client_key=SB-Mid-client-aQuCzf_xly4hqcSY#/' and @id = 'snap-midtrans' and @name = 'popup_1675684101759']</value>
      <webElementGuid>8c6f1484-b723-4a87-9e05-8607732e7da2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
