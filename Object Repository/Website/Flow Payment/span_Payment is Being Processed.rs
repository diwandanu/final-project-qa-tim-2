<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Payment is Being Processed</name>
   <tag></tag>
   <elementGuidId>9ef1d90f-e3e0-47d0-8746-bd8499932d95</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.badge.badge-warning</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='badge-status']/h5/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>c5a9144d-fc2d-4554-bdf6-0eea68c78b0c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>badge badge-warning</value>
      <webElementGuid>9094ec21-8ea6-4e97-b5ef-f4464a6ea5e3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Payment is Being Processed</value>
      <webElementGuid>a765abb0-079a-4fc1-bcd5-98a8b118c51b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;badge-status&quot;)/h5[1]/span[@class=&quot;badge badge-warning&quot;]</value>
      <webElementGuid>2d08a83a-91cc-4fe8-98a2-03e50289d348</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='badge-status']/h5/span</value>
      <webElementGuid>9a6ee3d2-553a-4cb9-81ac-24371723d5bb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='#EVN03022300001'])[1]/following::span[1]</value>
      <webElementGuid>e20cb627-b2c7-4662-875e-4f468e28125b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Back'])[1]/following::span[1]</value>
      <webElementGuid>bb64c7c1-706c-4634-9151-6ebf623bd6c3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bill To :'])[1]/preceding::span[1]</value>
      <webElementGuid>90245bbf-195e-4095-a86e-8d9f1f47d4c6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='D1m45 4ry4'])[1]/preceding::span[1]</value>
      <webElementGuid>1938c68c-7713-4998-b373-5a2bb08ee377</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Payment is Being Processed']/parent::*</value>
      <webElementGuid>f1b6e0f9-2681-483a-ae01-5164dfd18596</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h5/span</value>
      <webElementGuid>deff38ad-04af-4bb5-98bf-291d07226f65</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Payment is Being Processed' or . = 'Payment is Being Processed')]</value>
      <webElementGuid>848dd4ae-014d-4e54-afd6-4912fe63a105</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
