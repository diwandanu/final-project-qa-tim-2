<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Cancel                                   _4e9978</name>
   <tag></tag>
   <elementGuidId>db260b0c-c5c2-4119-be99-1b05abacd978</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#cancelButton</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='cancelButton']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>475eef38-cc84-4334-abc1-3a243ff3daac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>EVN08022300004/cancel</value>
      <webElementGuid>c06d1c30-b986-4f95-808f-c9cdb1d4a878</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-danger btn-icon icon-left</value>
      <webElementGuid>4828cc42-6678-4b9d-a978-7c95f2f45bfb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>cancelButton</value>
      <webElementGuid>8eb79eaf-902e-4530-af17-ba6b8260d0f0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Cancel
                                            Invoice</value>
      <webElementGuid>0d2c1662-8800-4255-a8b5-fc84a4a85b23</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;cancelButton&quot;)</value>
      <webElementGuid>16139b81-2d79-47f0-9019-94c7b65fe93b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@id='cancelButton']</value>
      <webElementGuid>8d32d941-c966-4286-9ffd-314599e36ac0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[3]/section/div/div/div/div/div[3]/div[3]/div/ul/a[2]</value>
      <webElementGuid>1e42595c-784d-4c67-9d3b-0c59500fdd37</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Cancel
                                            Invoice')]</value>
      <webElementGuid>33d3b736-6786-4e06-b99f-f7314e1ec8fb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Complete your payment here :'])[1]/following::a[2]</value>
      <webElementGuid>1797aee2-fc36-4226-b0d3-bc3c213f496a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Invoice Date :'])[1]/following::a[2]</value>
      <webElementGuid>eca6b1d4-1159-4ffe-884a-0cb9e2e6052f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Order Summary'])[1]/preceding::a[1]</value>
      <webElementGuid>fe18456d-b859-4ec2-a4d6-abd1352595b1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All items here cannot be deleted'])[1]/preceding::a[1]</value>
      <webElementGuid>4224c2ee-9b9d-43ca-b1e2-143df97193dc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'EVN08022300004/cancel')]</value>
      <webElementGuid>840c81e4-e55d-4e22-bd55-28cc62717297</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a[2]</value>
      <webElementGuid>0a5662be-5a8e-4961-aefb-5ae73d4398ad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'EVN08022300004/cancel' and @id = 'cancelButton' and (text() = ' Cancel
                                            Invoice' or . = ' Cancel
                                            Invoice')]</value>
      <webElementGuid>d6796ef1-0f1c-46d0-b36d-e8528591f2dc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'EVN07022300029/cancel')]</value>
      <webElementGuid>f845daaf-0d96-4823-863a-a24d97d91056</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'EVN07022300029/cancel' and @id = 'cancelButton' and (text() = ' Cancel
                                            Invoice' or . = ' Cancel
                                            Invoice')]</value>
      <webElementGuid>2df4b30c-882a-4245-b7dd-c0ee95bf19d2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'EVN07022300019/cancel')]</value>
      <webElementGuid>d4ce68c3-e2cd-437e-ab9a-00b1c93fdf9e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'EVN07022300019/cancel' and @id = 'cancelButton' and (text() = ' Cancel
                                            Invoice' or . = ' Cancel
                                            Invoice')]</value>
      <webElementGuid>cfc3e4e3-1bba-4417-93fe-ae91618ade76</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'EVN07022300013/cancel')]</value>
      <webElementGuid>5fb62b8c-b1d8-4148-9d76-2acb3b63e314</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'EVN07022300013/cancel' and @id = 'cancelButton' and (text() = ' Cancel
                                            Invoice' or . = ' Cancel
                                            Invoice')]</value>
      <webElementGuid>305d7c10-1949-48c9-851b-bab9399c7e83</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'EVN04022300008/cancel')]</value>
      <webElementGuid>7eddf97b-b6dd-4e1b-9b13-538155768955</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'EVN04022300008/cancel' and @id = 'cancelButton' and (text() = ' Cancel
                                            Invoice' or . = ' Cancel
                                            Invoice')]</value>
      <webElementGuid>9cc0c17a-ec0a-476d-b599-b58b6ac7f0e3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'EVN04022300007/cancel')]</value>
      <webElementGuid>eb47bcc8-6aed-4493-a50d-004487c88348</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'EVN04022300007/cancel' and @id = 'cancelButton' and (text() = ' Cancel
                                            Invoice' or . = ' Cancel
                                            Invoice')]</value>
      <webElementGuid>0b6393b7-54cf-4488-bada-75b21ac37934</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
