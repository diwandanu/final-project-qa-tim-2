<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_TEST_modal-iframe</name>
   <tag></tag>
   <elementGuidId>1c70fa52-5a3a-4362-bf19-3be722e0b611</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.modal-iframe</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//body[@id='snap-body']/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>c44463bd-22e7-4b51-85c1-62bd399eec51</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>modal-iframe</value>
      <webElementGuid>24d569f7-9e09-44a2-a09e-10345704c248</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;snap-body&quot;)/div[@class=&quot;modal-iframe&quot;]</value>
      <webElementGuid>5ac6a456-4184-42a7-a8aa-0bc3f6c3527e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Website/Flow Payment/iframe_concat(id(, , snap-midtrans, , ))_po_a5d0d2</value>
      <webElementGuid>2cda5413-1ebd-4309-9171-84ecfc1d5e67</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//body[@id='snap-body']/div</value>
      <webElementGuid>b7883897-1d95-41cf-9667-e87cc637d1a4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div</value>
      <webElementGuid>183c676b-5a61-4e24-aa9c-8c6df2417fe9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
