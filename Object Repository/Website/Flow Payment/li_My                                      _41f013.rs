<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_My                                      _41f013</name>
   <tag></tag>
   <elementGuidId>ad5838c0-5975-4898-b80a-07ffbb182f26</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='navbar-collapse-1']/ul/li[7]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>6b18ca65-037e-44d4-a13f-a5d05aa547a2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>___class_+?26___</value>
      <webElementGuid>46751db5-2262-42f6-9ebb-6e5440622c43</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                        
                                                    
                                                                                                                    My
                                                                    Course
                                                            
                                                            Checkout
                                                            
                                                            My Account
                                                                
                                                            
                                                                                                                Logout
                                                        
                                                    
                                                </value>
      <webElementGuid>49157e7f-a020-424a-a92e-55898cc3739d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;navbar-collapse-1&quot;)/ul[@class=&quot;nav navbar-nav&quot;]/li[@class=&quot;___class_+?26___&quot;]</value>
      <webElementGuid>e1e1380f-64be-4150-8533-f99412bed700</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='navbar-collapse-1']/ul/li[7]</value>
      <webElementGuid>5ba6ffab-8aac-4ac1-8890-51f2f261a849</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kontak'])[1]/following::li[1]</value>
      <webElementGuid>2517de40-729e-49b3-ba68-b477764b6040</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Blog'])[1]/following::li[2]</value>
      <webElementGuid>204140d6-8cb5-41d5-a203-bf948ebb7465</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[7]</value>
      <webElementGuid>3d351459-315d-48bb-91e5-624520c1ac1d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[(text() = '
                                                        
                                                    
                                                                                                                    My
                                                                    Course
                                                            
                                                            Checkout
                                                            
                                                            My Account
                                                                
                                                            
                                                                                                                Logout
                                                        
                                                    
                                                ' or . = '
                                                        
                                                    
                                                                                                                    My
                                                                    Course
                                                            
                                                            Checkout
                                                            
                                                            My Account
                                                                
                                                            
                                                                                                                Logout
                                                        
                                                    
                                                ')]</value>
      <webElementGuid>f20c007d-bfc5-4e64-ac45-667681dd06e4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
