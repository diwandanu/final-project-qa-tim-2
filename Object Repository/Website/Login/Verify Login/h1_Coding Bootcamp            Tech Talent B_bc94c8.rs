<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h1_Coding Bootcamp            Tech Talent B_bc94c8</name>
   <tag></tag>
   <elementGuidId>5c9bd241-55fa-4a5d-bfb0-985391bd76d9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h1.new_headline_h3</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='main-body-homepage']/div/h1</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h1</value>
      <webElementGuid>f55c7b82-f5ca-470f-ba25-f85a0c782789</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>new_headline_h3</value>
      <webElementGuid>06f9dbaa-e87e-4d90-a48d-d31beecbaef3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Coding Bootcamp
            Tech Talent Berkualitas</value>
      <webElementGuid>69be7d8e-e2fc-4db0-b749-0ac3ec6fdda9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-body-homepage&quot;)/div[@class=&quot;containerHeader&quot;]/h1[@class=&quot;new_headline_h3&quot;]</value>
      <webElementGuid>7780c209-b220-4054-91e4-aca5f2429ae3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-body-homepage']/div/h1</value>
      <webElementGuid>1e54abce-9990-4a91-9df5-ef0b4deca0e2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Quality Assurance Engineer Class'])[2]/preceding::h1[1]</value>
      <webElementGuid>370f6678-04ab-428e-a2c7-8aafef6bc948</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jadi QA Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.'])[1]/preceding::h1[1]</value>
      <webElementGuid>1083048f-daf4-4e3d-a575-48469bef30d4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Coding Bootcamp']/parent::*</value>
      <webElementGuid>efe70c7c-2a89-4612-8227-9134f142adcf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h1</value>
      <webElementGuid>1b5a9825-1b36-46c8-937f-8f4b626df49d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h1[(text() = 'Coding Bootcamp
            Tech Talent Berkualitas' or . = 'Coding Bootcamp
            Tech Talent Berkualitas')]</value>
      <webElementGuid>e32b4292-50a1-4668-a917-70a6f5a38ab9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
