import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

String folderApp = System.getProperty('user.dir')

Mobile.startApplication(folderApp + '/apk/DemoAppV2.apk', true)

Mobile.tap(findTestObject('Object Repository/Mobile/Register/btn_Login Here'), 0)

Mobile.tap(findTestObject('Object Repository/Mobile/Register/btn_Register Now'), 0)

Mobile.setText(findTestObject('Object Repository/Mobile/Register/box_Nama'), 'Muhamad Trian Diwandanu', 0)

Mobile.tap(findTestObject('Object Repository/Mobile/Register/btn_Calendar'), 0)

Mobile.tap(findTestObject('Object Repository/Mobile/Register/btn_Year'), 0)

Mobile.scrollToText('2004')

Mobile.scrollToText('2000')

Mobile.tap(findTestObject('Object Repository/Mobile/Register/temp/btn_Tahun 2000'), 0)

Mobile.tap(findTestObject('Object Repository/Mobile/Register/btn_Tanggal 25'), 0)

Mobile.tap(findTestObject('Object Repository/Mobile/Register/btn_Calendar OK'), 0)

Mobile.setText(findTestObject('Object Repository/Mobile/Register/box_Email'), 'triandiwandanu123@gmail.com', 0)

Mobile.setText(findTestObject('Object Repository/Mobile/Register/box_Whatsapp'), '081288527707', 0)

Mobile.tap(findTestObject('Object Repository/Mobile/Register/btn_Show Kata Sandi'), 0)

Mobile.setText(findTestObject('Object Repository/Mobile/Register/box_Kata Sandi'), 'pw123', 0)

Mobile.verifyElementExist(findTestObject('Object Repository/Mobile/Register/verify_Password must be alphanumeric min.8'), 
    0)

Mobile.closeApplication()

