import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

String folderApp = System.getProperty('user.dir')

Mobile.startApplication(folderApp + '/apk/DemoAppV2.apk', true)

Mobile.tap(findTestObject('Mobile/Register/btn_Login Here'), 0)

Mobile.setText(findTestObject('Object Repository/Mobile/Update/box_Login Email'), 'triandiwandanu17@gmail.com', 0)

Mobile.setText(findTestObject('Object Repository/Mobile/Update/box_Login Password'), '12345678', 0)

Mobile.tap(findTestObject('Object Repository/Mobile/Update/btn_Login'), 0)

Mobile.tap(findTestObject('Object Repository/Mobile/Update/btn_Logo Profile'), 0)

Mobile.tap(findTestObject('Object Repository/Mobile/Update/btn_Setting'), 0)

Mobile.tap(findTestObject('Object Repository/Mobile/Update/btn_Edit Profile'), 0)

Mobile.tap(findTestObject('Object Repository/Mobile/Update/btn_Calendar'), 0)

Mobile.tap(findTestObject('Object Repository/Mobile/Update/btn_Calendar Cancel'), 0)

Mobile.verifyElementExist(findTestObject('Object Repository/Mobile/Update/verify_Birthdate cant be empty'), 0)

Mobile.closeApplication()

