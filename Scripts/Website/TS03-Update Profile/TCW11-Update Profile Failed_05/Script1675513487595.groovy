import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://demo-app.online/')

WebUI.maximizeWindow()

WebUI.click(findTestObject('Object Repository/Website/Update Profile/a_Masuk'))

WebUI.setText(findTestObject('Object Repository/Website/Update Profile/Input Email'), 'dmsar141@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Website/Update Profile/Input Password'), 'cZFrDSk31FeaspcjiMwZ6g==')

WebUI.click(findTestObject('Object Repository/Website/Update Profile/button_Login'))

WebUI.click(findTestObject('Object Repository/Website/Update Profile/i_Kontak_fas fa-user-alt'))

WebUI.click(findTestObject('Object Repository/Website/Update Profile/a_My Account'))

WebUI.click(findTestObject('Object Repository/Website/Update Profile/span_Profil'))

WebUI.click(findTestObject('Object Repository/Website/Update Profile/a_Edit Profile'))

WebUI.setText(findTestObject('Object Repository/Website/Update Profile/Input Fullname'), 'dimas arya')

WebUI.setText(findTestObject('Website/Update Profile/Input Whatsapp Number'), '01234567891011')

WebUI.setText(findTestObject('Object Repository/Website/Update Profile/Input Birthday'), '19-Jan-2001')

WebUI.click(findTestObject('Object Repository/Website/Update Profile/button_Save Changes'))

'Notifikasi di kolom Phone\r\n'
WebUI.verifyTextPresent('The whatsapp must be between 10 and 12 digits.', true)

WebUI.delay(1)

WebUI.closeBrowser()

