import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://demo-app.online/')

WebUI.maximizeWindow()

WebUI.click(findTestObject('Object Repository/Website/Flow Payment/a_Masuk'))

WebUI.setText(findTestObject('Object Repository/Website/Flow Payment/input_Email_email'), 'triankpam@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Website/Flow Payment/input_Kata                                 _98da12'), 
    '7ht1qRXGbSG/2exrzA4yYA==')

WebUI.click(findTestObject('Object Repository/Website/Flow Payment/button_Login'))

WebUI.click(findTestObject('Object Repository/Website/Flow Payment/a_Events'))

WebUI.click(findTestObject('Object Repository/Website/Flow Payment/div_Day 3 Predict using Machine Learning   _e04a14'))

WebUI.click(findTestObject('Object Repository/Website/Flow Payment/a_Beli                                     _1a5f60'))

WebUI.click(findTestObject('Object Repository/Website/Flow Payment/a_Lihat Event Lainnya'))

WebUI.click(findTestObject('Object Repository/Website/Flow Payment/div_Day 4 Workshop                         _31d43a'))

WebUI.click(findTestObject('Object Repository/Website/Flow Payment/a_Beli                                     _1a5f60 (1)'))

WebUI.click(findTestObject('Object Repository/Website/Flow Payment/a_Lihat                Pembelian Saya'))

WebUI.click(findTestObject('Object Repository/Website/Flow Payment/input_Rp                                   _9c2b1a'))

WebUI.click(findTestObject('Object Repository/Website/Flow Payment/button_Checkout'))

WebUI.click(findTestObject('Object Repository/Website/Flow Payment/input_Total Pembayaran_payment_method'))

WebUI.click(findTestObject('Object Repository/Website/Flow Payment/button_Confirm'))

WebUI.click(findTestObject('Object Repository/Website/Flow Payment/img_PT Dwidata Talenta Prima_logo (4)'))

WebUI.verifyElementText(findTestObject('Object Repository/Website/Flow Payment/span_UNPAID'), 'UNPAID')

WebUI.click(findTestObject('Object Repository/Website/Flow Payment/li_My                                      _41f013'))

WebUI.click(findTestObject('Object Repository/Website/Flow Payment/a_My Account'))

WebUI.click(findTestObject('Object Repository/Website/Flow Payment/span_Invoice'))

WebUI.click(findTestObject('Object Repository/Website/Flow Payment/a_Detail'))

WebUI.click(findTestObject('Object Repository/Website/Flow Payment/a_Cancel                                   _4e9978'))

WebUI.click(findTestObject('Object Repository/Website/Flow Payment/button_OK'))

WebUI.closeBrowser()

