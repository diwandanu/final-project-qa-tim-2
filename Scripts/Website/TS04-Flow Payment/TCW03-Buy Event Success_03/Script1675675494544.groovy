import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://demo-app.online/')

WebUI.maximizeWindow()

WebUI.click(findTestObject('Website/Flow Payment/a_Masuk'))

WebUI.setText(findTestObject('Website/Flow Payment/input_Email_email'), 'triankpam@gmail.com')

WebUI.setEncryptedText(findTestObject('Website/Flow Payment/input_Kata                                 _98da12'), '7ht1qRXGbSG/2exrzA4yYA==')

WebUI.click(findTestObject('Website/Flow Payment/button_Login'))

WebUI.click(findTestObject('Website/Flow Payment/a_Events'))

WebUI.click(findTestObject('Website/Flow Payment/div_Day 3 Predict using Machine Learning   _e04a14'))

WebUI.click(findTestObject('Website/Flow Payment/a_Beli Tiket'))

WebUI.click(findTestObject('Website/Flow Payment/a_Lihat                Pembelian Saya'))

WebUI.click(findTestObject('Website/Flow Payment/button_Checkout'))

WebUI.click(findTestObject('Website/Flow Payment/input_Total Pembayaran_payment_method'))

WebUI.click(findTestObject('Website/Flow Payment/button_Confirm'))

WebUI.click(findTestObject('Website/Flow Payment/div_Indomaret_logo-border-payment-list'))

WebUI.verifyElementPresent(findTestObject('Website/Flow Payment/verify_Payment code'), 0)

WebUI.click(findTestObject('Object Repository/Website/Flow Payment/button_Close this page'))

WebUI.verifyElementPresent(findTestObject('Website/Flow Payment/verify_PAYMENT IS BEING PROCESSED'), 0)

WebUI.click(findTestObject('Object Repository/Website/Flow Payment/i_Kontak_fas fa-user-alt'))

WebUI.click(findTestObject('Object Repository/Website/Flow Payment/a_My Account'))

WebUI.click(findTestObject('Object Repository/Website/Flow Payment/a_Invoice'))

WebUI.click(findTestObject('Object Repository/Website/Flow Payment/a_Detail'))

WebUI.click(findTestObject('Object Repository/Website/Flow Payment/a_Cancel                                   _4e9978'))

WebUI.click(findTestObject('Object Repository/Website/Flow Payment/button_OK'))

WebUI.closeBrowser()

