import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://demo-app.online/')

WebUI.maximizeWindow()

WebUI.click(findTestObject('Object Repository/Website/Register/button_Buat Akun'))

WebUI.setText(findTestObject('Object Repository/Website/Register/Input Nama'), 'dimas')

WebUI.setText(findTestObject('Object Repository/Website/Register/Input BirthDay'), '19-Jan-2001')

WebUI.setText(findTestObject('Object Repository/Website/Register/Input E-mail'), 'qwert@gmail.com')

WebUI.setText(findTestObject('Object Repository/Website/Register/Input Whatsapp Number'), '8976543211')

WebUI.setEncryptedText(findTestObject('Object Repository/Website/Register/Input Password'), 'RigbBhfdqOBGNlJIWM1ClA==')

WebUI.setEncryptedText(findTestObject('Object Repository/Website/Register/Input Password Confirmation'), 'RigbBhfdqOBGNlJIWM1ClA==')

WebUI.click(findTestObject('Object Repository/Website/Register/Checkbox'))

WebUI.click(findTestObject('Object Repository/Website/Register/button_Daftar'))

WebUI.verifyElementText(findTestObject('Object Repository/Website/Register/Verify Register/span_Verifikasi Email'), 'Verifikasi Email')

WebUI.delay(1)

WebUI.closeBrowser()

