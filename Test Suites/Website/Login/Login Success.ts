<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Test Login using Excel Data</description>
   <name>Login Success</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>277061ee-d0ae-4b24-8aee-2e3f14e86781</testSuiteGuid>
   <testCaseLink>
      <guid>dfaf55b9-8fcf-4709-89b2-1c43649fc3f7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/TS02-Login/Excel TCW01-Login Success</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>97e72b91-ef5b-48ea-bdaa-8ea2ec1fa549</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Website/Data Login</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>97e72b91-ef5b-48ea-bdaa-8ea2ec1fa549</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>EmailExcel</value>
         <variableId>a1f781a5-20a2-4a2e-9e2d-2e9bf36f7ea0</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>97e72b91-ef5b-48ea-bdaa-8ea2ec1fa549</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>PasswordExcel</value>
         <variableId>35f638b1-aa46-40e7-b3cc-ff3d016a4b67</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
